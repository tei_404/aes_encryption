const express = require('express');
const path =require('path');
const app = express();
const LoggerMiddleware =(req ,res ,next)=>{
    console.log(`Logger ${req.url} ${res.method} -- ${new Date()}`);
    next();
}

app.use(LoggerMiddleware);
app.use(express.json());

//!set middle path  
app.use('/js',express.static(path.join(__dirname,'../src/js')))
app.use('/css',express.static(path.join(__dirname,'../src/css')))
app.use('/img',express.static(path.join(__dirname,'../src/img')))


 //! Routes
 const Index_Route = require('../route/Index_Route');
 app.use('/AES', Index_Route);

module.exports = app ;
